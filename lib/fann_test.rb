# frozen_string_literal: true

require_relative 'fann_test/version'
require 'ruby-fann'
require 'pry'

module FannTest
  class Error < StandardError; end

  data =
    Array.new(100_000).to_h do
      [
        input = Array.new(3).map { rand(1..10) }.sort!,
        Array.new(10, 0).tap { |array| array[(input.sum / input.size.to_f).round - 1] = 1 }
      ]
    end

  train = RubyFann::TrainData.new(
    inputs:          data.keys,
    desired_outputs: data.values
  )
  fann = RubyFann::Standard.new(
    num_inputs:     3,
    hidden_neurons: [10],
    num_outputs:    10
  )
  fann.train_on_data(train, 1000, 1000, 0.1) # 1000 max_epochs, 10 errors between reports and 0.1 desired MSE (mean-squared-error)

  input = Array.new(3).map { rand(1..10) }.sort!
  res   = (input.sum / input.size.to_f).round - 1
  p fann.run(input).each_with_index.max.last
  p res

  train.save('verify.train')
  train = RubyFann::TrainData.new(filename: 'verify.train')
  # Train again with 10000 max_epochs, 20 errors between reports and 0.01 desired MSE (mean-squared-error)
  # This will take longer:
  fann.train_on_data(train, 100_000, 1000, 0.01)

  fann.save('foo.net')
  saved_nn = RubyFann::Standard.new(filename: 'foo.net')
  p saved_nn.run(input).each_with_index.max.last
  p res
  # binding.pry
end
